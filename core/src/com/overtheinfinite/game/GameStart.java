package com.overtheinfinite.game;

import com.badlogic.gdx.ApplicationAdapter;

public class GameStart extends ApplicationAdapter {
	private GameManager gManager;
	public void create () {
		gManager = new GameManager();
		gManager.init();
	}

	public void render () {
		gManager.update();
		gManager.draw();
	}
}
