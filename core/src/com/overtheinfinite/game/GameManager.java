package com.overtheinfinite.game;

import java.io.IOException;
import java.sql.SQLException;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import main.java.com.overtheinfinite.newregion.controller.AppController;
import main.java.com.overtheinfinite.newregion.controller.CampaignApp;
import main.java.com.overtheinfinite.newregion.controller.MainMenu;
import main.java.com.overtheinfinite.newregion.controller.Scene;
import main.java.com.overtheinfinite.newregion.controller.UpdateResult;
import main.java.com.overtheinfinite.newregion.controller.UpdateType;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class GameManager {
	private Scene current;
	private TextButton main, campaign;
	
	public void init() {
		if(current == null) {
			current = new MainMenu();
		}
		if(current instanceof AppController) {
			DBM.getInstance().normalMode();
		}
		else if(current instanceof CampaignApp) {
			DBM.getInstance().campaignMode();
		}
		current.init();
	}

	public void draw() {
		if(current == null) {
			current = new MainMenu();
		}
		current.draw();
	}
	
	public void update() {
		UpdateResult result = current.update();
		if(result.getType() == UpdateType.justExit) {
			current = result.nextScene();
			init();
		}
	}
}
