package main.java.com.overtheinfinite.newregion.controller;

/**
 * when you touch items, selected image should be changed.
 * @author 허규남
 *
 */
public enum SelectedUpdateType {
	move, notchange, notvisible
}
