package main.java.com.overtheinfinite.newregion.controller.stage;

import java.sql.SQLException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import main.java.com.overtheinfinite.newregion.controller.SelectedUpdateType;
import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.game.element.FunctionButtonData;
import main.java.com.overtheinfinite.newregion.game.element.ViewData;
import main.java.com.overtheinfinite.newregion.game.worldmap.LocalMap;
import main.java.com.overtheinfinite.newregion.game.worldmap.StageUpdateResult;
import main.java.com.overtheinfinite.newregion.game.worldmap.WorldMap;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.ui.MapDrawer;
import main.java.com.overtheinfinite.newregion.ui.UIDrawer;
import main.java.com.overtheinfinite.newregion.ui.element.FunctionButton;

/**
 * for localmap
 * @author heoky
 *
 */
public class GameStage extends BasicStage {
	protected MapDrawer map;
	protected UIDrawer ui;
	
	private WorldMap worldMap;
	private LocalMap localMap;
	protected int map_id = 1;
	private Selected selected;
		
	public GameStage() throws SQLException {
		super();
		this.map = new MapDrawer();
		this.localMap = new LocalMap(this);
		this.worldMap = new WorldMap(dHelper);
		
		this.ui = new UIDrawer(skinManager.getBigSkin(), this.localMap.getBuilder());
		this.selected = new Selected();
		addActor(this.map);
		addActor(this.ui);
		addActor(this.worldMap);
		addActor(this.localMap);
		addActor(this.selected);
		this.selected.setVisible(false);
		
		updateMap(1);
	}
	
	public int getMapId() {
		return map_id;
	}
	
	
	public void gamePause(boolean value) {
		Touchable t = value? Touchable.disabled : Touchable.enabled;
		ui.setTouchable(t);
		map.setTouchable(t);
	}
	
	public void update() {
		Actor actor = hit();
		//this.selected.setVisible(false);
		SelectedUpdateType type = SelectedUpdateType.notchange;
		if(map_id == 0) {
			if(actor != null) {
				type = this.worldMap.onTouch(this, actor);
			}
			
			int new_map = this.worldMap.getNewMapId();
			if(new_map >= 0)
				updateMap(new_map);
		}
		else {
			type = this.localMap.onTouch(actor, this);
		}
		
		switch(type) {
		case move:
			this.selected.move(actor);
			break;
		case notvisible:
			this.selected.setVisible(false);
			break;
		default:
			break;
		}
		
	}
	
	public void setVisibleMode(boolean value) {
		this.localMap.setEnable(!value);
		
		//System.out.println(this.worldMap);
		this.worldMap.setVisible(value);
		this.worldMap.setTouchable(value? Touchable.enabled : Touchable.disabled);
	}
	
	public void updateMap(int map_id) {
		this.map_id = map_id;
		boolean isWorldMap = (map_id == 0);
		this.selected.setVisible(false);
		HistoryMaker hsmk = new HistoryMaker(DBM.getInstance().getDynamicDB());
		if(isWorldMap) {
			this.worldMap.update();
			hsmk.onEvent(HistoryMaker.EVENT_LOCALTOWORLD, 0);
		}
		else {
			this.localMap.update(map_id);
			hsmk.onEvent(HistoryMaker.EVENT_WORLDTOLOCAL, map_id);
		}
		setVisibleMode(isWorldMap);
		map.update(map_id);
		ui.clear();
	}

	
	/**
	 * 빈 타일을 터치했을 때 어떤 건물을 건설할 것인지에 대해 표시
	 * @param map
	 */
	public void viewBuildMenu(boolean isBuildCastle) {	
		ui.updateBuildButtons(this, isBuildCastle);
		
	}
	
	/**
	 * 해당 빌딩이 가지고 있는 고유 기능들을 표시
	 * @param data 빌딩에 대한 정보
	 * @throws SQLException
	 */
	public void viewFunctionMenu(ViewData[] views, FunctionButtonData[] btnDatas) throws SQLException {
		FunctionButton[] btns = new FunctionButton[btnDatas.length];
		
		for(int i = 0; i < btnDatas.length; i++) {
			btns[i] = new FunctionButton(btnDatas[i], skinManager.getBigSkin());
		}		
		
		ui.updateButton(skinManager.getBigSkin(), views, btns);
	}
	
	public void afterBuild() {
		this.selected.setVisible(false);
		this.ui.clear();
	}
}
