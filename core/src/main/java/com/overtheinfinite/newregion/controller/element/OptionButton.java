package main.java.com.overtheinfinite.newregion.controller.element;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class OptionButton extends TextButton {

	public OptionButton(String text, Skin skin) {
		super(text, skin);
	}

}
