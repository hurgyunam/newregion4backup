package main.java.com.overtheinfinite.newregion.controller;

import java.io.IOException;
import java.sql.SQLException;

import com.badlogic.gdx.Gdx;

import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class AppController implements Scene {
	protected DB sdb, ddb;
	protected GameStage stage;
	/**
	 * sdb,ddb들을 객체변수에 넣어주고 사용하기 편하도록 한다
	 * @throws SQLException
	 * @throws IOException
	 */
	public void init() {
		try {
			sdb = DBM.getInstance().getStaticDB();
			ddb = DBM.getInstance().getDynamicDB();
			stage = new GameStage();
			Gdx.input.setInputProcessor(stage);
		} catch (SQLException e) {
			throw new IllegalStateException("GameStage를 만드는데 에러가 발생함",e);
		}
	}
	@Override
	public void draw() {
		stage.act();
		stage.draw();
		stage.update();
	}
	
	@Override
	public UpdateResult update() {
		int ending = new HistoryMaker(DBM.getInstance().getDynamicDB()).getEventNumber(HistoryMaker.EVENT_ENDING);
		if(ending > 0) {
			return new UpdateResult(new MainMenu(), UpdateType.ending);
		}
		else {
			return new UpdateResult(null, UpdateType.playing);
		}
	}
}