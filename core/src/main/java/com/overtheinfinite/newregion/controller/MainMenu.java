package main.java.com.overtheinfinite.newregion.controller;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import main.java.com.overtheinfinite.newregion.controller.element.MainMenuButton;
import main.java.com.overtheinfinite.newregion.controller.element.MainMenuChoice;
import main.java.com.overtheinfinite.newregion.controller.stage.BasicStage;

public class MainMenu implements Scene {
	private Image background;
	private TextButton campaign, normal;
	private MainMenuChoice choice = MainMenuChoice.notChosen;
	private BasicStage stage;
	
	public MainMenu() {
	}
	
	@Override
	public void init() {
		stage = new BasicStage();
		campaign = new MainMenuButton(MainMenuChoice.campaign, stage.getSkinManager().getBigSkin());
		normal = new MainMenuButton(MainMenuChoice.infinite, stage.getSkinManager().getBigSkin());
		stage.addActor(campaign);
		stage.addActor(normal);
	}

	@Override
	public void draw() {
		stage.draw();
		Actor actor = stage.hit();
		if(actor != null) {
			//System.out.println(actor.getClass());
			//라벨이 선택되서 문제임
		}
		if(actor instanceof Label) {
			if(actor.getParent() instanceof MainMenuButton) {
				MainMenuButton mmbtn = (MainMenuButton) actor.getParent();
				this.choice = mmbtn.getChoice();
			}
		}
	}

	@Override
	public UpdateResult update() {
		switch(choice) {
		case notChosen:
			return new UpdateResult(null, UpdateType.playing);
		case campaign:
			return new UpdateResult(new CampaignApp(), UpdateType.justExit);
		case infinite:
			return new UpdateResult(new AppController(), UpdateType.justExit);
		default:
			throw new IllegalArgumentException("MainMenuChoice enum에 예상치못한 값이 들어감");
			
		}
	}
}
