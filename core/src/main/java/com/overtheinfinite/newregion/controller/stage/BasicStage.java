package main.java.com.overtheinfinite.newregion.controller.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class BasicStage extends Stage {
	protected SkinManager skinManager;
	protected DialogHelper dHelper;
	public BasicStage() {
		this.skinManager = new SkinManager(this);
		this.dHelper = new DialogHelper(this, skinManager.getSmallSkin());
	}
	
	public SkinManager getSkinManager() {
		return skinManager;
	}
	
	public DialogHelper getDialogHelper() {
		return dHelper;
	}
	
	public Actor hit() {
		if(Gdx.input.justTouched()) {
			Actor actor = hit(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY(), true);
			return actor;
		}
		else {
			return null;
		}
	}
}
