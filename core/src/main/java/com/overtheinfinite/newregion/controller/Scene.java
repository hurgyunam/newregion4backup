package main.java.com.overtheinfinite.newregion.controller;

public interface Scene {
	public void init();
	public void draw();
	public UpdateResult update();
}
