package main.java.com.overtheinfinite.newregion.controller.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.ui.MapDrawer;

public class Selected extends Image {
	public Selected() {
		super(new Texture(Gdx.files.internal("selected.png")));
		this.setSize(Gdx.graphics.getWidth() / MapDrawer.TILE_X_NUM, 
				Gdx.graphics.getHeight() / MapDrawer.TILE_Y_NUM);
	}
	
	public void move(Actor actor) {
		this.setVisible(true);
		this.setPosition(actor.getX(), actor.getY());
	}
}
