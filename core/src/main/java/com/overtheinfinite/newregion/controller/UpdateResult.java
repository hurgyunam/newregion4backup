package main.java.com.overtheinfinite.newregion.controller;

public class UpdateResult {
	private Scene nextScene;
	private UpdateType type;
	
	public UpdateResult(Scene nextScene, UpdateType type) {
		this.nextScene = nextScene;
		this.type = type;
	}
	
	public Scene nextScene() {
		return this.nextScene;
	}
	
	public UpdateType getType() {
		return this.type;
	}
}
