package main.java.com.overtheinfinite.newregion.controller.element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MainMenuButton extends TextButton {
	private static final String CAMPAIGN_TEXT = "캠페인모드", NORMAL_TEXT = "무한모드";
	public static final int CODE_CAMPAIGN = 101, CODE_NORMAL = 102;
	private static final int WIDTH = 300, HEIGHT = 60;
	private MainMenuChoice choice;
	public MainMenuButton(MainMenuChoice choice, Skin skin) {
		super("", skin);
		int halfWidth = Gdx.graphics.getWidth()/2 - WIDTH/2;
		int halfHeight = Gdx.graphics.getHeight()/2 - HEIGHT/2;
		switch(choice) {
		case campaign:
			setText(CAMPAIGN_TEXT);
			setBounds(halfWidth, 
					halfHeight - 50, 
					300, 80);
			break;
		case infinite:
			setText(NORMAL_TEXT);
			setBounds(halfWidth, 
					halfHeight - 150, 
					300, 80);
			break;
		}
		this.choice = choice;
	}
	
	public MainMenuChoice getChoice() {
		return choice;
	}

}
