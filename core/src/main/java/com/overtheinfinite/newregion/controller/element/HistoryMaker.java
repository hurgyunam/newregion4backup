package main.java.com.overtheinfinite.newregion.controller.element;

import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class HistoryMaker {
	public static final int EVENT_LOCALTOWORLD = 1, EVENT_WORLDTOLOCAL = 2, EVENT_WAR = 3, EVENT_ENDING = 4;
	private DB ddb;
	public HistoryMaker(DB ddb) {
		this.ddb = ddb;
	}
	public void onEvent(int event_type) {
		String historySQL = "insert into History(event_type, arg) values(?,?)";
		try {
			ddb.execute(historySQL, event_type, -1);
		} catch (SQLException e) {
			throw new IllegalStateException("히스토리를 추가하던 중 에러가 발생했습니다", e);
		}
	}
	
	public void onEvent(int event_type, int arg) {
		String historySQL = "insert into History(event_type, arg) values(?,?)";
		try {
			ddb.execute(historySQL, event_type, arg);
		} catch (SQLException e) {
			throw new IllegalStateException("히스토리를 추가하던 중 에러가 발생했습니다", e);
		}
	}
	
	public int getEventNumber(int event_type) {
		String countSQL = "select count(*) cnt from History where event_type = ?";
		try {
			return ddb.queryInteger(countSQL, event_type);
		} catch (SQLException e) {
			throw new IllegalArgumentException("히스토리를 추가하던 중 에러가 발생했습니다", e);
		}
	}
}
