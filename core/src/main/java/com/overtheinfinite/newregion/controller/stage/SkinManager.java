package main.java.com.overtheinfinite.newregion.controller.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;

import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;
import main.java.com.overtheinfinite.newregion.tools.dialog.TempDialog;

public class SkinManager {
	private static final String SKIN_NAME = "skin/assets/uiskin.json", 
			SMALL_FONT_NAME = "skin/assets/small.fnt",
			BIG_FONT_NAME = "skin/assets/default.fnt";
	private Skin smallSkin, bigSkin;
	private BitmapFont small, big;
	private Stage stage;
	
	public SkinManager(Stage stage) {

		this.small = new BitmapFont(Gdx.files.internal(SMALL_FONT_NAME));
		this.big = new BitmapFont(Gdx.files.internal(BIG_FONT_NAME));

		this.smallSkin = createSkin(this.small);
		this.bigSkin = createSkin(this.big);
		
		this.stage = stage;
	}
	
	public Skin createSkin(BitmapFont font) {
		Skin skin;
		skin = new Skin(Gdx.files.internal(SKIN_NAME));
		
		skin.get(LabelStyle.class).font = font;
		skin.get(WindowStyle.class).titleFont = font;
		skin.get(TextButtonStyle.class).font = font;
		skin.add("default-font", this.small);
		skin.add("big-font", this.big);
		return skin;
	}
	
	public Skin getSmallSkin() {
		return smallSkin;
	}
	
	public Skin getBigSkin() {
		return bigSkin;
	}
	
	public BitmapFont getSmallFont() {
		return small;
	}
	
	public BitmapFont getBigFont() {
		return big;
	}
}
