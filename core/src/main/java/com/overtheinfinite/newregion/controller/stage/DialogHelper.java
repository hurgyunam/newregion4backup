package main.java.com.overtheinfinite.newregion.controller.stage;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;
import main.java.com.overtheinfinite.newregion.tools.dialog.TempDialog;

public class DialogHelper {
	private Skin skin;
	private Stage stage;
	public DialogHelper(Stage stage, Skin skin) {
		this.stage = stage;
		this.skin = skin;
	}
	
	public TempDialog makeDialog(DialogParameter param) {
		TempDialog dialog = new TempDialog(param, skin);
		return dialog;
	}

	
	public void showDialog(DialogParameter param) {
		makeDialog(param).show(stage);
	}
}
