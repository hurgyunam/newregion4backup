package main.java.com.overtheinfinite.newregion.controller;

import java.sql.SQLException;

import com.badlogic.gdx.Gdx;
import com.overtheinfinite.game.GameManager;

import main.java.com.overtheinfinite.newregion.game.campaign.CampaignHelper;
import main.java.com.overtheinfinite.newregion.game.campaign.CharacterManager;
import main.java.com.overtheinfinite.newregion.game.campaign.MessageWindow;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;

public class CampaignApp extends AppController {
	private CampaignHelper helper;
	private CharacterManager manager;
	private MessageWindow window;
	private boolean isMessageMode = true;
	private boolean lastMsg = false, isEnding = false;
	
	public CampaignApp() {
	}
	
	@Override
	public void init() {
		try {
			super.init();
			helper = new CampaignHelper();
			manager = new CharacterManager();
			window = new MessageWindow(stage.getSkinManager().getSmallSkin());
			helper.load();
			stage.addActor(manager);
			stage.addActor(window);			

			window.update(helper.iterator());
			manager.update(window.getImage());
			window.next();
			setMessageMode(true);
			
			Logger.getInstance().addTags("campaign.condition.each");
		} catch (SQLException e) {
			throw new IllegalStateException("캠페인을 로드하던중 오류발생", e);
		}
	}
	
	public void setMessageMode(boolean value) {
		window.setVisible(value);
		manager.setVisible(value);
		stage.gamePause(value);
		isMessageMode = value;
	}
	
	@Override
	public void draw() {
		super.draw();
		if(isMessageMode) {
			if(Gdx.input.justTouched()) {
				manager.update(window.getImage());
				if(window.next() == false) {
					setMessageMode(false);
					if(lastMsg) {
						stage.getDialogHelper().showDialog(new DialogParameter("모든 캠페인을 완료하였습니다", "확인"));
						DBM.getInstance().getDynamicDB().delete();
						isEnding = true;
					}
				}
			}
		}
		else {
			if(helper.checkCondition()) {
				try {
					if(helper.nextLevel() == false) {
						//game ending;
						lastMsg = true;
					}
					else {
						window.update(helper.iterator());
						setMessageMode(true);
					}
				} catch (SQLException e) {
					throw new IllegalStateException("캠페인 데이터 불러오던 중 에러", e);
				}
				
			}
		}
	}
	
	@Override
	public UpdateResult update() {
		if(isEnding) {
			return new UpdateResult(new MainMenu(), UpdateType.ending);
		}
		else {
			return new UpdateResult(null, UpdateType.playing);
		}
	}
}
