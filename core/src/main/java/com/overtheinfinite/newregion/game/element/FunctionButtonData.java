package main.java.com.overtheinfinite.newregion.game.element;

public class FunctionButtonData {
	private String filename, label;
	private int function_id, static_building_id, building_id;
	public FunctionButtonData(String filename, String label, 
			int function_id, int static_building_id, int building_id) {
		this.filename = filename;
		this.label = label;
		this.function_id = function_id;
		this.static_building_id = static_building_id;
		this.building_id = building_id;
	}
	public String getFilename() {
		return filename;
	}
	public String getLabel() {
		return label;
	}
	public int getFunctionId() {
		return function_id;
	}
	public int getStatic_buildingId() {
		return static_building_id;
	}
	public int getBuildingId() {
		return building_id;
	}
	
	
}
