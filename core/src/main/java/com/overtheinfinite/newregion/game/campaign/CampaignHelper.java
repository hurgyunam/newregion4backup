
package main.java.com.overtheinfinite.newregion.game.campaign;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class CampaignHelper {
	private static final int MISSION_RESOURCE = 1, MISSION_BUILDING = 2, MISSION_WORLDMAP = 3, MISSION_WAR = 4;
	private ArrayList<MessageData> datas = new ArrayList<MessageData>();
	private CampaignMission campaign_mission;
	private int campaign = -1, mission = -1;
	private DB ddb, sdb;

	/**
	 * @throws SQLException
	 * @throws IOException
	 */
	public CampaignHelper() {
		this.ddb = DBM.getInstance().getDynamicDB();
		this.sdb = DBM.getInstance().getStaticDB();
		this.campaign_mission = new CampaignMission(this.ddb);
	}
	
	/**
	 * 현재 캠페인이 진행된 상황을 처음으로 초기화한다.
	 * @throws SQLException
	 */
	public void clear() throws SQLException {
		ResultSet set = ddb.query("select * from Campaign;");
		if(set.isClosed()) {
			//we need 1 campaign row, so make it
			ddb.execute("insert into Campaign(user_id, campaign_id, mission_id) values (?,?,?);"
					,1, 1, 1);
		}
		else {
			ddb.execute("update Campaign set user_id = ?, campaign_id = ?, mission_id = ?;"
					, 1, 1, 1);
		}
	}
	//처음에 어디까지 진행했는지는 알아야 하니까
	//없으면 새로 만들고
	/**
	 * 캠페인이 어디까지 진행되었는지 확인하고 이를 반영한다
	 * @return
	 * @throws SQLException
	 */
	public boolean load() throws SQLException {
		ResultSet set = ddb.query("select * from Campaign;");
		if(set.next() == false) {
			//if db doesn't have row, it must has one
			ddb.execute("insert into Campaign(user_id, campaign_id, mission_id) values (?,?,?);"
					,1, 1, 1);
			return false;
		}
		else {
			//if exists, load the data
			this.campaign = set.getInt("campaign_id");
			this.mission = set.getInt("mission_id");
			
			//message data
			loadMessageData();
			return true;
		}
	}
	
	/**
	 * 캠페인시 캐릭터들의 대화 데이터에 대해 불러온다
	 * @return
	 * @throws SQLException
	 */
	public boolean loadMessageData() throws SQLException {
		datas.clear();
		if(mission == 1) {
			//chapter intro
			ResultSet titleSet = sdb.query("select * from Campaign where campaign_id = ?;", campaign);
			String title = titleSet.getString("name");
			datas.add(new MessageData("chapter" + campaign, 0, title, 0));
		}
		ResultSet msgSet = sdb.query("select m.actor, m.message, m.image, m.message_id"
				+ " from CampaignMessage m"
				+ " where campaign_id = ? and chapter_id = ? "
				+ " order by message_id;", this.campaign, this.mission);
		while(msgSet.next()) {
			String actor = msgSet.getString("actor");
			String msg = msgSet.getString("message");
			int image = msgSet.getInt("image");
			int msg_id = msgSet.getInt("message_id");
			MessageData data = new MessageData(actor, image, msg, msg_id);
			Logger.getInstance().add("campaign.load", data.toString());
			datas.add(data);
		}
		MessageData data = campaign_mission.getCampaignLabel(this.sdb);
		if(data != null) {
			datas.add(data);
			Logger.getInstance().add("CampaignHelper", data.toString());
		}
		
		return datas.size() > 0;
	}
	
	/**
	 * 퀘스트 조건에 부합하는지 확인한다
	 * @return 모든 조건이 부합하는 경우 true를 반환한다.
	 * @throws SQLException
	 */
	//마지막 미션 판정일경우 메시지만 뱉은 후 조건이 없으므로 true를 반환한다. 쉽게 넘어갈 수 있다
	public boolean checkCondition() {
		try {
			ResultSet missionSet = sdb.query("select * from CampaignMission where campaign_id = ? and mission_id = ?;"
					, this.campaign, this.mission);
			Logger.getInstance().add("campaign.condition", 
					"campaign " + this.campaign
					+ " mission " + this.mission);
			boolean value = true;
			while(missionSet.next()) {
				Logger.getInstance().add("campaign.condition", 
						"it has event " + missionSet.getInt("event"));
				value = value && checkConditionEach(missionSet);
			}
			return value;
		}catch(SQLException e) {
			throw new IllegalStateException("조건을 확인하다 에러가 발생", e);
		}
	}
	
	private boolean checkConditionEach(ResultSet missionSet) throws SQLException {
		int event = missionSet.getInt("event");
		int data_id = missionSet.getInt("data_id");
		int num = missionSet.getInt("number");
		int condition = missionSet.getInt("condition");
		
		boolean result = campaign_mission.mission(event, data_id, num, condition);
		return result;
	}
	/**
	 * 다음 미션으로 넘어갈때 필요한 데이터들을 넣어준다
	 * @return
	 * @throws SQLException
	 */
	public boolean nextLevel() throws SQLException {
		this.mission++;
		if(!checkLevel(this.campaign, this.mission)) {
			this.mission = 1;
			this.campaign++;
			if(!checkLevel(this.campaign, this.mission)) {
				return false;
			}
		}
		updateLevel(this.campaign, this.mission);
		loadMessageData();
		return true; 
	}
	
	/**
	 * @param campaign
	 * @param mission
	 * @return if the level exists return true;
	 * @throws SQLException
	 */
	public boolean checkLevel(int campaign, int mission) throws SQLException {
		String checkSQL = "select name from CampaignMission"
				+ " where campaign_id = ?"
				+ " and mission_id = ?";
		ResultSet checkSet = sdb.query(checkSQL, campaign, mission);
		
		return checkSet.next();
	}
	/**
	 * update campaign number and delete history
	 * @param campaign
	 * @param mission
	 * @throws SQLException
	 */
	public void updateLevel(int campaign, int mission) throws SQLException {
		ddb.execute("update Campaign set campaign_id = ?, mission_id = ?"
				, campaign, mission);
		//mission이 끝났으므로 히스토리를 초기화 시켜준다
		ddb.execute("delete from History");
	}
	
	public Iterator<MessageData> iterator() {
		return datas.iterator();
	}
	
	public int getMissionNumber() {
		return this.mission;
	}
	
	public int getCampaignNumber() {
		return this.campaign;
	}
}
