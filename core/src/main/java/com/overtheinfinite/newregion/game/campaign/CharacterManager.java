package main.java.com.overtheinfinite.newregion.game.campaign;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.tools.Logger;


public class CharacterManager extends Group {
	private static final int CAMPAIGN_NOBLE = 37;
	private Image[] images;
	private LinkedList<Image> queue;
	public CharacterManager() {
		images = new Image[3];
		images[0] = new Image(new Texture(Gdx.files.internal("campaign/noble.png")));
		images[1] = new Image(new Texture(Gdx.files.internal("campaign/prince.png")));
		images[2] = new Image(new Texture(Gdx.files.internal("campaign/woman.png")));
		queue = new LinkedList<Image>();
		for(int i = 0; i < images.length; i++) {
			addActor(images[i]);
			images[i].setColor(0, 0, 0, 0);
		}
	}
	
	public boolean update(int image) {
		//여기서 이제 인묻들의 위치를 잘 조정하면 될 것 같다		
		int index = image - CAMPAIGN_NOBLE;
		Logger.getInstance().add("CharacterManager", image + "");
		if(index >= 0) {
			queue.remove(images[index]);
			queue.add(images[index]);
			
			//위치 조정
			//최신 캐릭터가 왼쪽에 나오고 그 다음은 오른쪽에 약간 어둡게 나오고 그 이후는 안보여야지
			int size = queue.size();
			
			if(size > 0) {
				queue.get(size-1).setBounds(0, 50, 300, 500);
				queue.get(size-1).setColor(1, 1, 1, 1);
			}
			if(size > 1) {
				queue.get(size-2).setBounds(Gdx.graphics.getWidth(), 50, -300, 500);
				queue.get(size-2).setColor(0.5f, 0.5f, 0.5f, 0.5f);
			}
			if(size > 2) {
				queue.get(size-3).setColor(0f, 0f, 0f, 0f);
			}
			
			return true;
		}
		return false;
	}
}
