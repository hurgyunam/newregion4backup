package main.java.com.overtheinfinite.newregion.game.worldmap;

public class WarResult {
	private int money;
	private boolean isWin;
	public WarResult(boolean isWin, int money) {
		this.isWin = isWin;
		this.money = money;
	}
	
	public boolean isWin() {
		return isWin;
	}
	
	public int money() {
		return money;
	}
	
	public String toString() {
		if(isWin) {
			return "당신은 승리하였으며, " + money + "G 를 착취하였습니다";
		}
		else {
			return "당신은 패배하였으며, 모든 병력이 전멸했습니다";
		}
	}
}
