package main.java.com.overtheinfinite.newregion.game.function;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.game.worldmap.Enemy;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.ui.element.Reason;

public class FunctionNormal {
	private DB sdb, ddb;
	private Reason reason;
	public FunctionNormal(DB sdb, DB ddb) {
		this.sdb = sdb;
		this.ddb = ddb;
		this.reason = new Reason();
	}
	
	public Reason getReason() {
		return reason;
	}
	
	public boolean checkCondition(ResultSet funcSet, boolean isBenefitted) throws SQLException {
		while(funcSet.next()) {
			int lack_type = funcSet.getInt("lack_type");
			if(lack_type == 1) {
				int rsrc = funcSet.getInt("resource_id");
				//check the number what we required
				int value = getValue(funcSet, isBenefitted);
				
				String lackSQL = "select number from Resource"
						+ " where resource_id = ?";
				ResultSet lackSet = this.ddb.query(lackSQL, rsrc);
				lackSet.next();
				Logger.getInstance().add("execute", 
						"lack resource : " + rsrc
						+ "/" + lackSet.getInt("number")
						+ "+" + value);
				
				//current my resource < required resource
				int lack = lackSet.getInt("number") + value;
				if(lack < 0) {
					String name = sdb.queryString("select name from Resource where resource_id = ?", rsrc);
					reason.setReason(name + "이(가) " + ((-1)*lack) + "만큼 부족합니다");
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param building_id ddb building
	 * @return
	 * @throws SQLException
	 */
	public boolean isBenefitted(int building_id) throws SQLException {
		String buildingSQL = "select isBenefitted from Building"
				+ " where building_id = ?";
		ResultSet buildingSet = this.ddb.query(buildingSQL, building_id);
		return (buildingSet.getInt("isBenefitted") == 1);
	}
	
	public ResultSet funcSet(int function_id) throws SQLException {
		String funcSQL = "select resource_id, value, add_value, type, lack_type"
				+ " from BuildingFunction"
				+ " where function_id = ?";
		return this.sdb.query(funcSQL, function_id);
	}
	/**
	 * normally used function
	 * @param function_id sdb id
	 * @param building_id ddb id
	 * @return
	 * @throws SQLException
	 */
	public boolean execute(int function_id, int building_id) throws SQLException {
		//int function_id = data.getId();
		ResultSet funcSet = funcSet(function_id);

		//1. are we use about add_value?
		boolean isBenefitted = isBenefitted(building_id);
		
		//2. if lack_type exists, do this satistied the condition?
		if(checkCondition(funcSet, isBenefitted)) {
			funcSet = funcSet(function_id);
			
			//3. resource add
			addResource(funcSet, isBenefitted);
			reason.setReason("기능이 성공적으로 수행되었습니다");
			return true;
		}
		return false;
	}
	
	/**
	 * @param funcSet resource_id, value, add_value
	 * @param isBenefitted
	 * @throws SQLException
	 */
	public void addResource(ResultSet funcSet, boolean isBenefitted) throws SQLException {
		while(funcSet.next()) {
			Logger.getInstance().add("execute", 
					"add resource : " + funcSet.getInt("resource_id"));
			addValue(funcSet, isBenefitted);
		}		
	}
	
	/**
	 * @param funcSet resource_id, value, add_value ResultSet
	 * @param isBenefitted can this tile earn addtional resource 
	 * @throws SQLException
	 */
	public void addValue(ResultSet funcSet, boolean isBenefitted) throws SQLException {
		int rsrc = funcSet.getInt("resource_id");
		//basic value
		int value = getValue(funcSet, isBenefitted);
		
		Logger.getInstance().add("FunctionNormal", "addValue " + rsrc + " : " + value);
		String updateSQL = "update Resource"
				+ " set number = number + ?"
				+ " where resource_id = ?";
		this.ddb.execute(updateSQL, value, rsrc);
		
		if(rsrc == Resource.ARMOR_LEVEL || rsrc == Resource.SOLDIER)
			Resource.update();
	}
	
	/**
	 * �ǹ��� ������ ������ ���� �ִ����� �ľ��� ��Ȯ�� value���� ��ȯ
	 * @param funcSet
	 * @param isBenefitted
	 * @return
	 * @throws SQLException
	 */
	public int getValue(ResultSet funcSet, boolean isBenefitted) throws SQLException {
		int value = funcSet.getInt("value");
		//can it earn the addtional point?
		if(isBenefitted) {
			int add_value = funcSet.getInt("add_value");
			value += add_value;
		}
		return value;
	}
	
	public void init(int static_building_id, boolean isBenefitted) throws SQLException {
		String funcSQL = "select resource_id, value, add_value, type from BuildingFunction"
				+ " where building_id = ?"
				+ " and type = ?";
		ResultSet funcSet = sdb.query(funcSQL, static_building_id, Building.FUNC_INIT);
		while(funcSet.next()) {
			addValue(funcSet, isBenefitted);
		}
	}
	
	public boolean checkHasHuman(int static_building_id) throws SQLException {
		String humanSQL = "select human from Building where building_id = ?";
		int human_number = sdb.queryInteger(humanSQL, static_building_id);
		String numberSQL = "select number from Resource where resource_id = ?";
		int has_number = ddb.queryInteger(numberSQL, Resource.REST_HUMAN);
		boolean returnVal = has_number >= human_number;
		return returnVal;
	}
	
	public void pushTheHuman(int static_building_id) throws SQLException {
		String humanSQL = "select human from Building where building_id = ?";
		int human_number = sdb.queryInteger(humanSQL, static_building_id);
		String pushSQL = "update Resource set number = number + ? where resource_id = ?";
		ddb.execute(pushSQL, human_number, Resource.WORK_HUMAN);
		Resource.update();
	}
}
