package main.java.com.overtheinfinite.newregion.game;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class Resource {
	public static final int SOLDIER = 1, MONEY = 2, MEAT = 3, BREAD = 4,
			HAPPINESS = 5, ARMOR_LEVEL = 6, MAX_HUMAN = 7,
			WORK_HUMAN = 8, REST_HUMAN = 9, POOR_HUMAN = 10,
			NORMAL_HUMAN = 12, SOLDIER_POWER = 13;
	
	public static int getNumber(int id) throws SQLException {
		DB ddb = DBM.getInstance().getDynamicDB();
		String getSQL = "select number from Resource where resource_id = ?";
		ResultSet set = ddb.query(getSQL, id);
		set.next();
		return set.getInt("number");
	}
	
	public static void setNumber(int id, int value) throws SQLException {
		DB ddb = DBM.getInstance().getDynamicDB();
		String updateSQL = "update Resource set number = ? where resource_id = ?";
		ddb.execute(updateSQL, value, id);
	}
	
	public static void addNumber(int id, int value) throws SQLException {
		DB ddb = DBM.getInstance().getDynamicDB();
		String updateSQL = "update Resource set number = number + ? where resource_id = ?";
		ddb.execute(updateSQL, value, id);
	}
	
	public static void update() throws SQLException {
		int max_human = getNumber(Resource.MAX_HUMAN);
		int soldier = getNumber(Resource.SOLDIER);
		setNumber(Resource.NORMAL_HUMAN, max_human - soldier);
		
		int level = getNumber(Resource.ARMOR_LEVEL);
		int number = getNumber(Resource.SOLDIER);
		setNumber(Resource.SOLDIER_POWER, level * number);
		
		int work = getNumber(Resource.WORK_HUMAN);
		setNumber(Resource.REST_HUMAN, max_human - soldier - work);
	}
}
