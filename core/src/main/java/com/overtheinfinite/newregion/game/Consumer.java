package main.java.com.overtheinfinite.newregion.game;

import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;

//i think it's real small data.
//and data format can't cover many data
//so i decide we don't use maintain table
//and we just consume it by code
//and we won't use human
public class Consumer {
	/** difference divide it and consume*/
	public static final int CONSUME_DENOMINATOR = 10;
	private DB ddb;
	public Consumer(DB ddb) {
		this.ddb = ddb;
	}
	
	public int consume() {
		int lack = consumeBread();
		lack += consumeMeat();
		consumeHappiness(lack);
		return lack;
	}
	//if world lack bread. consume happiness
	public int consumeBread() {
		try {
			int bread = Resource.getNumber(Resource.BREAD);
			int human = Resource.getNumber(Resource.NORMAL_HUMAN);
			
			int consume = human / CONSUME_DENOMINATOR;
			return consumeEach(consume, Resource.BREAD, bread);
			/*
			if(consume > bread) {
				ddb.execute("update Resource set number = 0"
						+ " where resource_id = ?",
						Resource.BREAD);
				return consume - bread;
			}
			else {
				ddb.execute("update Resource set number = number - ?"
						+ " where resource_id = ?",
						consume,
						Resource.BREAD);
				return 0;
			}		*/	
		} catch (SQLException e) {
			throw new IllegalStateException("빵을 소비하던 중 에러가 발생", e);
		}
	}
	
	public int consumeMeat() {
		try {
			int soldier = Resource.getNumber(Resource.SOLDIER);
			int meat = Resource.getNumber(Resource.MEAT);
			
			int consume = soldier / CONSUME_DENOMINATOR;
			
			return consumeEach(consume, Resource.MEAT, meat);			
		}catch(SQLException e) {
			throw new IllegalStateException("고기를 소비하던 중 에러가 발생", e);
		}
	}
	
	public int consumeEach(int consume, int id, int value) throws SQLException {
		if(consume > value) {
			ddb.execute("update Resource set number = 0"
					+ " where resource_id = ?",
					id);
			return consume - value;
		}
		else {
			ddb.execute("update Resource set number = number - ?"
					+ " where resource_id = ?",
					consume,
					id);
			return 0;
		}
	}
	
	public boolean consumeHappiness(int lack)  {
		try {
			int happiness = Resource.getNumber( Resource.HAPPINESS);
			int happylack = consumeEach(lack, Resource.HAPPINESS, happiness);
			return happylack > 0;
		} catch (SQLException e) {
			throw new IllegalStateException("행복을 소비하던 중 에러 발생", e);
		}
	}
	
	public boolean checkHappiness(DialogHelper dHelper){
		try {
			if(Resource.getNumber(Resource.HAPPINESS) <= 0) {
				dHelper.showDialog(new DialogParameter("행복이 낮습니다. 교회를 건설하세요", "확인"));
				return false;
			}
			return true;
		} catch (SQLException e) {
			throw new IllegalStateException("행복을 계산하던 중 에러가 발생", e);
		}
	}
}
