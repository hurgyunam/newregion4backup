package main.java.com.overtheinfinite.newregion.game.element;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.db.SQLiteManager;

public class ViewData {
	private String filename, label;
	private int data_id;
	
	public ViewData(String filename, String label, int data) {
		this.filename = filename;
		this.label = label;
		this.data_id = data;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public int getDataId() {
		return data_id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public int getResourceValueByDataId() throws SQLException {
		SQLiteManager ddb = DBM.getInstance().getDynamicDB();
		ResultSet set = ddb.query("select number from Resource where resource_id = ?;",data_id);
		set.next();
		return set.getInt("number");
	}
	
	public String toString() {
		try {
			return this.label + "*" + getResourceValueByDataId();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalArgumentException("리소스 id가 잘못 들어간 것으로 추정", e);
		}
	}
}
