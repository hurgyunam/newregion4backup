package main.java.com.overtheinfinite.newregion.game.worldmap;

import main.java.com.overtheinfinite.newregion.controller.SelectedUpdateType;

public class StageUpdateResult {
	private SelectedUpdateType selectedRefresh;
	private boolean uiRefresh;	
	
	public StageUpdateResult(SelectedUpdateType selectedRefresh, boolean uiRefresh) {
		this.selectedRefresh = selectedRefresh;
		this.uiRefresh = uiRefresh;
	}
	
	public SelectedUpdateType isSelectedRefresh() {
		return selectedRefresh;
	}
	
	public boolean isUiRefresh() {
		return uiRefresh;
	}
}
