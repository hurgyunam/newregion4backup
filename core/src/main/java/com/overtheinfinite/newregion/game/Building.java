package main.java.com.overtheinfinite.newregion.game;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.game.element.BuildingData;
import main.java.com.overtheinfinite.newregion.game.element.FunctionButtonData;
import main.java.com.overtheinfinite.newregion.game.element.ViewData;
import main.java.com.overtheinfinite.newregion.game.function.FunctionNextTurn;
import main.java.com.overtheinfinite.newregion.game.function.FunctionNormal;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.ui.element.Reason;

/**
 * [TODO] 
 * 1. 월드맵 이동 기능
 * 2. nextTurn 함수
 * 3. onTouch 함수
 */
public class Building {
	private DB sdb, ddb;
	public static final int FUNC_PASSIVE = 1, FUNC_INIT = 2, FUNC_NEXTTURN = 3,
			FUNC_NORMAL = 4, FUNC_WORLDMAP = 5, FUNC_TRAIN = 6;
	private Reason reason = new Reason();
	private FunctionNextTurn func_nextturn;
	private FunctionNormal func_normal;
	private GameStage stage;
	
	public Building(GameStage stage) throws SQLException {
		this.sdb = DBM.getInstance().getStaticDB();
		this.ddb = DBM.getInstance().getDynamicDB();
		this.func_nextturn = new FunctionNextTurn(sdb, ddb);
		this.func_normal = new FunctionNormal(sdb, ddb);
		this.stage = stage;
	}
	
	public Reason getReason() {
		return reason;
	}
	
	public ViewData[] getViewData(int static_building_id) throws SQLException {
		String countSQL = "select count(*) cnt from BuildingView where building_id = ?";
		int count = this.sdb.queryInteger(countSQL, static_building_id);
		ViewData[] views = new ViewData[count];
		
		//load about view data
		String viewSQL = "select v.view_id view, r.resource_id rsrc, r.image image, r.name name"
				+ " from BuildingView v, Resource r"
				+ " where v.resource_id = r.resource_id"
				+ " and v.building_id = ?";
		ResultSet viewSet = this.sdb.query(viewSQL, static_building_id);
		for(int i = 0; viewSet.next(); i++) {
			views[i] = new ViewData(
					viewSet.getString("image"),
					viewSet.getString("name"),
					viewSet.getInt("rsrc"));
			//resource 개수는 ddb라 처리하기 복잡함.
			//viewData 내에서 해주는게 좋음
		}
		
		return views;
	}
	
	public int getStaticBuildingId(int building_id) throws SQLException {
		String buildingSQL = "select building_kind_id from Building where building_id = ?";
		ResultSet buildingSet = this.ddb.query(buildingSQL, building_id);
		buildingSet.next();
		
		return buildingSet.getInt("building_kind_id");
	}
	
	
	/**
	 * ddb의 빌딩 id를 입력해주면 이에 필요한 뷰와 버튼 데이터를 반환해준다
	 * @param views
	 * @param btns
	 * @param building_id ddb의 빌딩 id
	 * @throws SQLException
	 */
	public FunctionButtonData[] getFunctionButtonData(int static_building_id, int building_id) throws SQLException {
		//load about button data
		String countSQL = "select count(*) cnt from BuildingFunction where building_id = ?"
				+ " group by function_id";
		ResultSet countSet = this.sdb.query(countSQL, static_building_id);
		int count;
		for(count=0;countSet.next();count++);
		FunctionButtonData[] btns = new FunctionButtonData[count];
		
		String btnSQL = "select f.function_id func,"
				+ " f.name name, i.image_name image"
				+ " from BuildingFunction f, Imagename i"
				+ " where f.button_image_id = i.image_id"
				+ " and f.building_id = ?"
				+ " group by f.function_id";
		ResultSet btnSet = this.sdb.query(btnSQL, static_building_id);
		//btnSet.next();
		for(int i = 0; btnSet.next(); i++) {
			//Logger.getInstance().add("getBuildingData", btnSet.getString("name"));
			btns[i] = new FunctionButtonData(
					btnSet.getString("image"),
					btnSet.getString("name"),
					btnSet.getInt("func"),
					static_building_id,
					building_id);
		}
		
		return btns;
	}
	
	
	//building_id : ddb상 실질적인 빌딩 id
	/**
	 * 다음 턴에 대한 기능을 처리하는 함수
	 * @throws SQLException
	 */	
	public void nextTurn() throws SQLException {
		func_nextturn.nextTurn();
	}
	
	

	//내가 어느 버튼을 선택했는가?
	//빌딩 기능 실행만 이곳에 따라옴.
	/**
	 * 건물 기능 실행과 관련된 내용임
	 * @param btn type 
	 * 
	 */
	public boolean onTouch(FunctionButtonData btn) throws SQLException {
		// TODO Auto-generated method stub
		return execute(btn);
	}

	/**
	 * 빌딩에서 특정 버튼을 눌렀을 때 이에 대해 처리해주는 것
	 * @param function_id sdb상 빌딩 기능 id
	 * @param building_id ddb상 실질적인 빌딩 id
	 * @return
	 * @throws SQLException
	 */
	
	public boolean execute(int function_id, int building_id) throws SQLException {
		//1. find what the type of this function
		String functionSQL = "select type"
				+ " from BuildingFunction"
				+ " where function_id = ?";
		ResultSet functionSet = this.sdb.query(functionSQL, 
				function_id);
		while(functionSet.next()) {
			int type = functionSet.getInt("type");
			Logger.getInstance().add("Building.execute", "function type : " + type);
			if(type == FUNC_NORMAL) {
				boolean value = func_normal.execute(function_id, building_id);
				this.reason = func_normal.getReason();
				return value;
			}
			else if(type == FUNC_WORLDMAP) {
				//stage와 연관된 처리임
				this.reason.setReason("월드맵으로 이동했습니다");
				new HistoryMaker(ddb).onEvent(HistoryMaker.EVENT_LOCALTOWORLD);
				stage.updateMap(0);
				return true;
			}
			else if(type == FUNC_NEXTTURN) {
				this.reason.setReason("다음턴으로 넘어갔습니다");
				nextTurn();
				return true;
			}
		}
		//reason : passive, init
		reason.setReason("자동으로 수행되는 기능입니다");
		return false;
	}
	/**
	 * 타입이 어떤지에 따라 처리를 해줌
	 * @param btn
	 * @return
	 * @throws SQLException
	 */
	public boolean execute(FunctionButtonData btn) throws SQLException {
		return execute(btn.getFunctionId(), btn.getBuildingId());
	}
	
	public BuildingData[] getBuildingDatas(int map_id) throws SQLException {
		LinkedList<BuildingData> list = new LinkedList<BuildingData>();
		//1. 빌딩들의 위치 등의 기능은 ddb에
		//2. 빌딩의 이미지 파일은 sdb에
		
		String posSQL = "select x,y,building_id,building_kind_id from Building where map_id = ?";
		ResultSet posSet = ddb.query(posSQL, map_id);
		
		while(posSet.next()) {
			String imgFileSQL = "select i.image_name image"
					+ " from Building b, ImageName i"
					+ " where b.image = i.image_id"
					+ " and b.building_id = ?";
			ResultSet imgFileSet = sdb.query(imgFileSQL, posSet.getInt("building_kind_id"));
			imgFileSet.next();
			BuildingData data = new BuildingData(imgFileSet.getString("image"), posSet.getInt("building_id"),
					posSet.getInt("x"), posSet.getInt("y"));
			list.add(data);
		}
		
		BuildingData[] arr = new BuildingData[list.size()];
		list.toArray(arr);
		return arr;
	}
}