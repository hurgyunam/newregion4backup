package main.java.com.overtheinfinite.newregion.game.worldmap;

import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;

public class WarDialogListener implements DialogListener {
	private Enemy enemy;
	private DialogHelper dHelper;
	private WorldMap worldMap;
	private int map_id;
	private int enemyPower, myPower;
	public WarDialogListener(WorldMap worldMap, DialogHelper dHelper, int map_id) {
		this.map_id = map_id;
		this.worldMap = worldMap;
		this.enemy = new Enemy(DBM.getInstance().getDynamicDB());
		this.dHelper = dHelper;
		try {
			enemyPower = enemy.getEnemyPower(map_id);
			myPower = enemy.getMyPower();
		} catch (SQLException e) {
			throw new IllegalStateException("", e);
		}
	}
	
	public String toString() {
		return "전쟁하시겠습니까? (" + myPower + "=>" + enemyPower + ")";
	}

	@Override
	public void onTouchButton(boolean isOK, int id) {
		if(isOK) {
			try {
				HistoryMaker hsmk = new HistoryMaker(DBM.getInstance().getDynamicDB());
				WarResult result = enemy.fight(myPower, enemyPower, map_id);
				hsmk.onEvent(HistoryMaker.EVENT_WAR, result.isWin()? 1:0);
				if(enemy.checkWin()) {
					hsmk.onEvent(HistoryMaker.EVENT_ENDING);
				}
				dHelper.showDialog(new DialogParameter(result.toString(), "확인"));
				worldMap.update();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}
}
