package main.java.com.overtheinfinite.newregion.game.worldmap;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.tools.db.DB;

public class Enemy {
	private DB ddb;
	public Enemy(DB ddb) {
		this.ddb = ddb;
	}
	
	public void nextTurn() throws SQLException {
		String updateSQL = "update WorldRegion set turn = turn + 1";
		ddb.execute(updateSQL);
		String checkSQL = "select map_id from WorldRegion"
				+ " where add_turn = turn";
		ResultSet checkSet = ddb.query(checkSQL);
		while(checkSet.next()) {
			String addSQL = "update WorldRegion set number = number + add_value"
					+ " where map_id = ?";
			ddb.execute(addSQL, checkSet.getInt("map_id"));
		}
	}
	
	public int getEnemyPower(int map_id) throws SQLException {
		String enemySQL = "select number * power power from WorldRegion where map_id = ?";
		ResultSet enemySet = ddb.query(enemySQL, map_id);
		enemySet.next();
		return enemySet.getInt("power");
	}
	
	public int getMyPower() throws SQLException {
		String mySQL = "select number from Resource where resource_id = ?";
		ResultSet mySet = ddb.query(mySQL, Resource.SOLDIER_POWER);
		mySet.next();
		return mySet.getInt("number");
	}
	
	public boolean fightEach(int myPower, int enemyPower, int map_id) throws SQLException {
		int dif = myPower - enemyPower;
		if(myPower < enemyPower) {
			dif *= -1;
			//minus enemy number but think power
			//my soldier number set zero
			
			String minusEnemySQL = "update WorldRegion"
					+ " set number = number - ( ? / power )"
					+ " where map_id = ?";
			ddb.execute(minusEnemySQL, dif, map_id);
			
			String zeroMySQL = "update Resource"
					+ " set number = 0 where resource_id = ?";
			ddb.execute(zeroMySQL, Resource.SOLDIER);

			Resource.update();
			
			return false;
		}
		else {
			//delete world region by map_id
			//minus my soldier but think armor level
			//update my power
			
			String deleteEnemySQL = "delete from WorldRegion where map_id = ?";
			ddb.execute(deleteEnemySQL, map_id);
			
			
			int armor = Resource.getNumber(Resource.ARMOR_LEVEL);
			
			int minus = enemyPower / armor;
			
			String minusMySQL = "update Resource set number = number - ?"
					+ " where resource_id = ?";
			ddb.execute(minusMySQL, minus, Resource.SOLDIER);
			
			Resource.update();
			
			return true;
		}
	}
	
	public WarResult fight(int myPower, int enemyPower, int map_id) throws SQLException {
		int money = enemyPower / 2;
		if(fightEach(myPower, enemyPower, map_id)) {
			String moneySQL = "update Resource set number = number + ?"
					+ " where resource_id = ?";
			ddb.execute(moneySQL, money, Resource.MONEY);
			
			return new WarResult(true, money);
		}
		else {
			String moneySQL = "update Resource set number = number - ?"
					+ " where resource_id = ?";
			ddb.execute(moneySQL, money, Resource.MONEY);
			String checkSQL = "update Resource set number = 0"
					+ " where resource_id = ? and number < 0";
			ddb.execute(checkSQL, Resource.MONEY);
			return new WarResult(false, 0);
		}
	}
	
	public boolean checkWin() throws SQLException {
		String regionSQL = "select count(*) cnt from WorldRegion";
		ResultSet regionSet = ddb.query(regionSQL);
		regionSet.next();
		return regionSet.getInt("cnt") == 0;
	}
}
