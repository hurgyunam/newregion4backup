package main.java.com.overtheinfinite.newregion.game.element;

import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;

public class BuildButtonData {
	private String filename, label;
	private int static_building_id;
	private boolean enabled;
	
	public BuildButtonData(String filename, String label, 
			int static_building_id, boolean enabled) {
		this.filename = filename;
		this.label = label;
		this.static_building_id = static_building_id;
		this.enabled = enabled;
	}
	public BuildButtonData(String filename, String label, 
			int static_building_id) {
		this(filename, label, static_building_id, true);
	}

	public String getFilename() {
		return filename;
	}

	public String getLabel() {
		return label;
	}

	public int getStaticBuildingId() {
		return static_building_id;
	}
	
	public boolean isEnable() {
		return enabled;
	}
	
	public int getMyPrice() throws SQLException {
		return DBM.getInstance().getDynamicDB().queryInteger(
				"select number from Resource where resource_id = ?;", 
				Resource.MONEY);
	}
	
	public int getPrice() throws SQLException {
		return DBM.getInstance().getStaticDB().queryInteger( 
				"select money from Building where building_id = ?"
				, this.static_building_id);
	}
}
