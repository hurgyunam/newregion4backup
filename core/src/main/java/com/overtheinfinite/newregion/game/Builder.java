package main.java.com.overtheinfinite.newregion.game;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import main.java.com.overtheinfinite.newregion.game.element.BuildButtonData;
import main.java.com.overtheinfinite.newregion.game.element.TileData;
import main.java.com.overtheinfinite.newregion.game.function.FunctionNormal;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.ui.element.Reason;

public class Builder {
	private static final int BUILDING_CASTLE = 2;
	private DB sdb, ddb;
	private Reason reason = new Reason();
	private TileData tile;
	private BuildButtonData buildData;

	public Builder() throws SQLException {
		this.sdb = DBM.getInstance().getStaticDB();
		this.ddb = DBM.getInstance().getDynamicDB();
	}
	
	public Reason getReason() {
		return reason;
	}
	
	public void onTileTouch(TileData tile) {
		this.tile = tile;
	}

	/**빈 타일을 터치해 빌딩을 건설해야할 때 이에 걸맞는 값으로
	 * 
	 * @return
	 * @throws SQLException
	 */
	public BuildButtonData[] getAllBuildingData() throws SQLException {
		String getAllBuildingSQL = "select b.building_id id"
				+ ", b.name name, i.image_name image"
				+ " from Building b, ImageName i"
				+ " where b.button_image_id = i.image_id"
				+ " order by id";
		ResultSet allBuildingSet = this.sdb.query(getAllBuildingSQL);
		LinkedList<BuildButtonData> datas = new LinkedList<BuildButtonData>();
		while(allBuildingSet.next()) {
			BuildButtonData temp = new BuildButtonData( 
					allBuildingSet.getString("image"),
					allBuildingSet.getString("name"),
					//sdb임
					allBuildingSet.getInt("id"));
			datas.add(temp);
		}
		BuildButtonData[] btns = new BuildButtonData[datas.size()];
		datas.toArray(btns);
		return btns;
	}
	
	public boolean isBuildCastle(int map_id) {
		if(map_id == 0) 
			throw new IllegalArgumentException("성이 지어져있는가 체크를 하는건 로컬맵에서만 가능");
		String checkSQL = "select count(*) cnt from Building where map_id = ? and building_kind_id = ?";
		int count;
		try {
			count = ddb.queryInteger(checkSQL, map_id, BUILDING_CASTLE);
			return count > 0;
		} catch (SQLException e) {
			throw new IllegalArgumentException("성의 존재 유무를 파악하기 위해 DB를 탐색하던 중 에러 발생", e);
		}
	}
	
	public boolean build(BuildButtonData data, int map_id, int x, int y) throws SQLException {
		FunctionNormal fn = new FunctionNormal(sdb, ddb);
		//1. how many rest human rested?
		if(fn.checkHasHuman(data.getStaticBuildingId()) == false) {
			String resultStr = "모든 인원이 건물에 할당되어 있습니다. 숙소를 추가로 건설하세요";
			reason.setReason(resultStr);
			return false;
		}
		
		//2. kind_id를 추출했으니 이를 통해 해당 빌딩 건설 가격을 파악하자
		String moneySQL = "select money"
				+ " from Building"
				+ " where building_id = ?";
		ResultSet moneySet = sdb.query(moneySQL, data.getStaticBuildingId());
		int money = moneySet.getInt("money");
		
		//3. 난 지금 충분한 돈을 가지고 있는가?
		String havingSQL = "select number from Resource"
				+ " where resource_id = ?";
		ResultSet havingSet = ddb.query(havingSQL, Resource.MONEY);
		int havingMoney = havingSet.getInt("number");

		//4. 돈이 없으면 못만들지
		if(havingMoney < money) {
			String resultStr = "현재 " + havingMoney + "의 금액이 있으며, " + money + "만큼 필요합니다.";
			reason.setReason(resultStr);
			Logger.getInstance().add("Builder.reason", resultStr);
			return false;
		}
		
		//5. 돈을 감소시키자
		String paySQL = "update Resource set number = number - ?"
				+ " where resource_id = ?";
		ddb.execute(paySQL, money, Resource.MONEY);
		
		//6. 건물을 추가하자
		String buildSQL = "insert into Building"
				+ "(building_kind_id, x, y, map_id, isBenefitted) values (?,?,?,?,?);";

		//7. terrainSet을 통해 이득을 볼 수 있는 건물인지 확인한다.
		//애초에 ExtendedBuilding에 포함이 안될수 있으므로 next로도 우선 체크를 해야한다.
		String terrainSQL = "select terrain_id t_id from ExtendedBuilding"
				+ " where building_id = ?";
		ResultSet terrainSet = sdb.query(terrainSQL, data.getStaticBuildingId());
		
		boolean isBenefitted = false;
		if(terrainSet.next()) {
			isBenefitted = terrainSet.getInt("t_id") == tile.getType();
		}
		ddb.execute(buildSQL, data.getStaticBuildingId(), x, y
				, map_id, isBenefitted? 1:0);
		
		// INIT에 대한 처리를 해줌
		fn.init(data.getStaticBuildingId(), isBenefitted);
		
		fn.pushTheHuman(data.getStaticBuildingId());
		
		return true;
	}
	
	public boolean build(BuildButtonData btn) throws SQLException  {
		// TODO Auto-generated method stub
		if(tile == null) return false;
		return build(btn, tile.getMapId(), tile.getX(), tile.getY());
	}
	
	public void onTouch(BuildButtonData btn) {
		this.buildData = btn;
	}

	public boolean build() throws SQLException {
		if(tile == null) return false;
		if(buildData == null) return false;
		return build(this.buildData, tile.getMapId(), tile.getX(), tile.getY());
	}
}