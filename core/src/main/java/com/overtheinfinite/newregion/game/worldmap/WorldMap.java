package main.java.com.overtheinfinite.newregion.game.worldmap;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import main.java.com.overtheinfinite.newregion.controller.SelectedUpdateType;
import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.controller.stage.SkinManager;
import main.java.com.overtheinfinite.newregion.game.Consumer;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;

//elements : users building, ai building
public class WorldMap extends Group {
	private Enemy enemySystem;
	private Texture my, enemy;
	private DB ddb;
	private Consumer consumer;
	private DialogHelper dHelper;
	private int newMapId = -1;
	
	public WorldMap(DialogHelper dHelper) throws SQLException {
		this.my = new Texture(Gdx.files.internal("structure/castle.png"));
		this.enemy = new Texture(Gdx.files.internal("structure/castle_blue.png"));
		this.ddb = DBM.getInstance().getDynamicDB();
		this.enemySystem = new Enemy(ddb);
		this.consumer = new Consumer(ddb);
		this.dHelper = dHelper;
	}
	
	public void update() {
		try {
			clear();
			findEnemyBuilding();
			findMyBuilding();
		}catch(SQLException e) {
			throw new IllegalStateException("월드맵 건물을 업데이트 하던중 SQL 에러가 발생했습니다", e);
		}
	}
	
	public void findMyBuilding() throws SQLException {
		String xySQL = "select map_id from Building group by map_id";
		ResultSet xySet = ddb.query(xySQL);
		while(xySet.next()) {
			WorldRegion wr = new WorldRegion(my, xySet.getInt("map_id"), false);
			addActor(wr);
		}
	}
	
	public void findEnemyBuilding() throws SQLException {
		String xySQL = "select map_id from WorldRegion";
		ResultSet xySet = ddb.query(xySQL);
		while(xySet.next()) {
			WorldRegion wr = new WorldRegion(enemy, xySet.getInt("map_id"), true);
			addActor(wr);
		}
	}
	
	/**
	 * 
	 * @param stage
	 * @param actor
	 * @return
	 */
	public SelectedUpdateType onTouch(GameStage stage, Actor actor) {
		if(actor instanceof WorldRegion) {
			//touch enemy building and my building
			WorldRegion wr = (WorldRegion) actor;
			if(wr.isEnemy()) {
				//when touch enemy, fight enemy if your happiness is enough
				if(consumer.checkHappiness(dHelper)) {
					wr.onTouchEnemy(dHelper, enemySystem, this);
				}
			}
			else {
				//when touch my house, go to the local map, and write the history
				wr.onTouchMe(stage, dHelper);
				new HistoryMaker(ddb).onEvent(HistoryMaker.EVENT_WORLDTOLOCAL);
			}
			return SelectedUpdateType.move;
		}
		else if(actor instanceof MapImage){
			//touch the blank tile for build the castle
			MapImage image = (MapImage) actor;
			
			MapChangeDialogListener mcdl = new MapChangeDialogListener(stage, dHelper, image.getData().getMapId());
			DialogParameter param = new DialogParameter("해당 미 개척지로 이동하시겠습니까?", "확인", "취소", 
					mcdl, 0);
			dHelper.showDialog(param);
			
			return SelectedUpdateType.notvisible;
		}
		//touch blank
		return SelectedUpdateType.notchange;
	}
	
	public int getNewMapId() {
		int value = this.newMapId;
		this.newMapId = -1;
		return value;
	}
}
