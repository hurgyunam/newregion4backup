package main.java.com.overtheinfinite.newregion.game.worldmap;

import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;

public class MapChangeDialogListener implements DialogListener {
	private GameStage stage;
	private DialogHelper dhelper;
	private int map_id;
	public MapChangeDialogListener(GameStage stage, DialogHelper dhelper, int map_id) {
		this.stage = stage;
		this.dhelper = dhelper;
		this.map_id = map_id;
	}
	@Override
	public void onTouchButton(boolean isOK, int id) {
		if(isOK) {
			DialogParameter param = new DialogParameter("이동을 완료했습니다", "확인");
			dhelper.showDialog(param);
			new HistoryMaker(DBM.getInstance().getDynamicDB()).onEvent(HistoryMaker.EVENT_WORLDTOLOCAL);
			stage.updateMap(map_id);
		}
	}

}
