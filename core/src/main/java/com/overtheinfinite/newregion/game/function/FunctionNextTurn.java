package main.java.com.overtheinfinite.newregion.game.function;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.Consumer;
import main.java.com.overtheinfinite.newregion.game.worldmap.Enemy;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.db.DB;

public class FunctionNextTurn {
	private static final int RESERVATION_TRAIN = 1, RESERVATION_UPGRADE = 2;
	private FunctionNormal normal_func;
	private Enemy enemy;
	private Consumer consumer;
	private DB sdb, ddb;
	public FunctionNextTurn(DB sdb, DB ddb) {
		this.sdb = sdb;
		this.ddb = ddb;
		this.normal_func = new FunctionNormal(sdb, ddb);
		this.enemy = new Enemy(ddb);
		this.consumer = new Consumer(ddb);
	}
	public void nextTurn() throws SQLException {
		String buildingSQL = "select building_id, building_kind_id from Building";
		ResultSet buildingSet = ddb.query(buildingSQL);
		
		while(buildingSet.next()) {
			//2.
			String funcSQL = "select resource_id, type, value, add_value "
					+ " from BuildingFunction where building_id = ?"
					+ " and type = ?";
			ResultSet funcSet = sdb.query(funcSQL, 
					buildingSet.getInt("building_kind_id"),
					Building.FUNC_PASSIVE);
			
			int building_id = buildingSet.getInt("building_id");
			boolean isBenefitted = normal_func.isBenefitted(building_id);
			//train이나 reservation은 더 이상 사용하지 않는다
			while(funcSet.next()) {
				int type = funcSet.getInt("type");
				Logger.getInstance().add("FunctionNextTurn", type + "");
				normal_func.addValue(funcSet, isBenefitted);
			}
		}
		
		//add enemies soldier and power
		enemy.nextTurn();
		
		//human consume bread, meat, happiness
		consumer.consume();
	}
	/**
	 * @param funcSet resource, value, add_value
	 * @param isBenefitted
	 * @throws SQLException
	 */
	/*
	public void ntTrain(ResultSet funcSet, boolean isBenefitted) throws SQLException {
		String reservationSQL = "select type from Reservation order by reservation_id";
		ResultSet reservationSet = ddb.query(reservationSQL);
		while(reservationSet.next()) {
			normal_func.addValue(funcSet, isBenefitted);
		}
		
		String removeSQL = "delete from Reservation";
		ddb.execute(removeSQL);
	}
	
	public void train() throws SQLException {
		String reservationSQL = "insert into Reservation(type) values(?)";
		ddb.execute(reservationSQL, RESERVATION_TRAIN);
	}
	*/
}
