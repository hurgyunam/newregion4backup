package main.java.com.overtheinfinite.newregion.game.worldmap;

import java.sql.SQLException;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.controller.element.HistoryMaker;
import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.tools.db.DBM;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;

public class WorldRegion extends Image{
	private int map_id;
	private boolean isEnemy;
	public WorldRegion(Texture tex, int map_id, boolean isEnemy) {
		super(tex);
		int x = (map_id-1) % 5;
		int y = (map_id-1) / 5;
		this.setBounds(x * MapImage.XSIZE, y * MapImage.YSIZE, 
				MapImage.XSIZE, MapImage.YSIZE );
		this.map_id = map_id;
		this.isEnemy = isEnemy;
	}
	
	public boolean isEnemy() {
		return isEnemy;
	}
	
	public void onTouchEnemy(DialogHelper dHelper, Enemy enemy, WorldMap worldMap) {
		WarDialogListener wdl = new WarDialogListener(worldMap, dHelper, map_id);
		dHelper.showDialog(new DialogParameter(wdl.toString(), "전쟁", "취소", wdl));	
	}
	
	public void onTouchMe(GameStage stage, DialogHelper dhelper) {
		MapChangeDialogListener mcdl = new MapChangeDialogListener(stage, dhelper, map_id);
		dhelper.showDialog(new DialogParameter(
				"해당 로컬 맵으로 이동하시겠습니까?", 
				"예", "아니오", mcdl, 0));
	}
}
