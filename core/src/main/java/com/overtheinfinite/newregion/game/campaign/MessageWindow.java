package main.java.com.overtheinfinite.newregion.game.campaign;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import main.java.com.overtheinfinite.newregion.tools.Logger;

public class MessageWindow extends Window {
	private Iterator<MessageData> iterator;
	private MessageData recent;
	private boolean iteratorEnd = false;
	public MessageWindow(Skin skin) {
		super("", skin);
		pad(10).align(Align.topLeft);
		setWidth(Gdx.graphics.getWidth());
		Logger.getInstance().add("MessageWindow", getWidth() + ":" + getHeight());
		setBounds(0, 0, getWidth(), getHeight());
		addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				Logger.getInstance().add("MessageWindow.OnTouch", "OnTouch ");
			}
		});
	}
	
	public void update(Iterator<MessageData> it) {
		iterator = it;
		recent = it.next();
		iteratorEnd = false;
	}
	public boolean next() {
		clear();
		if(iteratorEnd)
			return false;
		String actor = recent.getActor();
		add("[" + actor + "]");
		row();
		while(actor.equals(recent.getActor())) {
			add(recent.getMsg());
			Logger.getInstance().add("MessageWindow.next", recent.getMsg());
			row();
			if(iterator.hasNext()) {
				recent = iterator.next();
			}
			else {
				//만약 마지막 행이 있었더라도 다음 문자가 없다는 이유로
				//캔슬이 되면 안됨
				iteratorEnd = true;
				break;
			}
		}
		return true;
	}
	
	public String getActor() {
		return recent.getActor();
	}
	public int getImage() {
		return recent.getImage();
	}
}
