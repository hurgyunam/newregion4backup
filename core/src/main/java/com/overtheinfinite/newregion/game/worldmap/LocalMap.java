package main.java.com.overtheinfinite.newregion.game.worldmap;

import java.sql.SQLException;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import main.java.com.overtheinfinite.newregion.controller.SelectedUpdateType;
import main.java.com.overtheinfinite.newregion.controller.stage.DialogHelper;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.game.Builder;
import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.game.element.BuildingData;
import main.java.com.overtheinfinite.newregion.game.element.FunctionButtonData;
import main.java.com.overtheinfinite.newregion.game.element.ViewData;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogParameter;
import main.java.com.overtheinfinite.newregion.ui.element.BuildButton;
import main.java.com.overtheinfinite.newregion.ui.element.FunctionButton;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;

public class LocalMap extends Group implements DialogListener {	
	private BuildingData[] datas;
	private Building building;
	private Builder builder;
	private DialogHelper dhelper;
	private GameStage stage;
	private int map_id = -1;
	
	public LocalMap(GameStage stage) {
		try {
			this.builder = new Builder();
			this.building = new Building(stage);
			this.dhelper = stage.getDialogHelper();
			this.stage = stage;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void update(int map_id) {
		try {
			clear();
			this.map_id = map_id;
			this.datas = building.getBuildingDatas(map_id);
			for(int i = 0; i < datas.length; i++) {
				addActor(datas[i]);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setEnable(boolean value) {
		setVisible(value);
		setTouchable(value? Touchable.enabled : Touchable.disabled);
	}
	
	/**
	 * 
	 * @param actor
	 * @param stage
	 * @return 
	 */
	public SelectedUpdateType onTouch(Actor actor, GameStage stage) {
		if(actor != null) {
			if(actor instanceof MapImage) {
				//맵 타일 터치 => 빌드메뉴 표시해줘야 함
				MapImage map = (MapImage) actor;
				builder.onTileTouch(map.getData());
				//빌드 메뉴 = UI
				stage.viewBuildMenu(builder.isBuildCastle(this.map_id));
				//selected.move(actor);
				return SelectedUpdateType.move; //need to refresh selected
			}
			else if(actor instanceof BuildingData) {
				//건물을 선택함 => 해당 기능을 보게끔 해야함
				try {
					BuildingData data = (BuildingData) actor;
					
					int staticId = building.getStaticBuildingId(data.getId());
					ViewData[] views = building.getViewData(staticId);
					FunctionButtonData[] btns = building.getFunctionButtonData(staticId, data.getId());
					
					stage.viewFunctionMenu(views, btns);
					builder.onTileTouch(null);
					return SelectedUpdateType.move; //need to refresh selected
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			else if(actor instanceof BuildButton) {
				//빌드메뉴에서 원하는 건물을 선택함
				//빌드 = 건설한다는 소리니 selected 없애야함
				BuildButton btn = (BuildButton) actor;
				builder.onTouch(btn.getData());
				DialogParameter param = new DialogParameter(btn.toString(), "건설", "취소", this);
				dhelper.showDialog(param);
				return SelectedUpdateType.notchange;
			}
			else if(actor.getParent() instanceof FunctionButton) {
				//이건 아마 맞을 것이다 그때 이미 해보았다
				//
				try {
					FunctionButton btn = (FunctionButton) actor.getParent();
					boolean value = building.execute(btn.getButtonData());
					Logger.getInstance().add("FunctionButton", "execute result : " + value);
					String reason = building.getReason().getReason();
					
					
					DialogParameter param = new DialogParameter(reason, "확인");
					dhelper.showDialog(param);
					
					return SelectedUpdateType.notchange;
				} catch (SQLException e) {
					throw new IllegalArgumentException("빌딩의 기능을 수행하던 도중 에러가 발생", e);
				}
			}
		}
		return SelectedUpdateType.notchange;
	}
	
	public Builder getBuilder() {
		return builder;
	}
	
	public Building getBuilding() {
		return building;
	}
	
	@Override
	public void onTouchButton(boolean isOK, int id) {
		if(isOK) {
			try {
				boolean value = builder.build();
				DialogParameter param;
				if(value) {
					Logger.getInstance().add("BuildButton.onTouch", "we just build it");
					//drawer.update();
					//건물을 갱신하기 위해 만든 메소드. 로컬맵을 갱신하면 된다
					builder.onTileTouch(null);
					Resource.update();
					
					if(this.map_id >= 0)
						update(this.map_id);
					
					param = new DialogParameter("건설되었습니다", "확인");
					stage.afterBuild();
				}
				else {
					
					param = new DialogParameter(builder.getReason().getReason(), "확인");
				}
				dhelper.showDialog(param);
			}
			catch(SQLException e) {
				throw new IllegalArgumentException("건물을 건설하던 중 에러가 발생", e);
			}
		}
	}
}
