package main.java.com.overtheinfinite.newregion.game.element;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;

public class BuildingData extends Image {
	private String filename;
	private int x,y, id;
	public BuildingData(String filename, int id, int x, int y) {
		super(new Texture(filename));
		Logger.getInstance().add("BuildingData", x + ":" + y);
		this.filename = filename;
		this.id = id;
		this.x = x;
		this.y = y;
		setBounds(x * MapImage.XSIZE, y * MapImage.YSIZE, MapImage.XSIZE, MapImage.YSIZE);
	}
	public String getFilename() {
		return filename;
	}
	public int getTileX() {
		return x;
	}
	public int getTileY() {
		return y;
	}
	public int getId() {
		return id;
	}
}
