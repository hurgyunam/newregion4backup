package main.java.com.overtheinfinite.newregion.game.campaign;

public class MessageData {
	private String actor, msg;
	private int image, message_id;

	public MessageData(String actor, int image, String msg, int message_id) {
		this.msg = msg;
		this.image = image;
		this.actor = actor;
		this.message_id = message_id;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getImage() {
		return image;
	}

	public void setImageFile(int image) {
		this.image = image;
	}

	public String getActor() {
		return actor;
	}
	public int getMessageId() {
		return message_id;
	}
	public void setActor(String actor) {
		this.actor = actor;
	}
	public String toString() {
		return "[" + actor + "] : " + msg;
	}
}
