package main.java.com.overtheinfinite.newregion.game.campaign;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.java.com.overtheinfinite.newregion.tools.db.DB;

public class CampaignMission {
	private static final int MISSION_RESOURCE = 1, MISSION_BUILDING = 2, MISSION_WORLDMAP = 3, MISSION_WAR = 4;
	public static final int CONDITION_MORE = 1, CONDITION_LESS = 2;
	public static final int EVENT_LOCALTOWORLD = 1, EVENT_WORLDTOLOCAL = 2, EVENT_WAR = 3;
	private DB ddb;
	public CampaignMission(DB ddb) {
		this.ddb = ddb;
	}
	
	public boolean mission(int event, int data_id, int number, int condition) throws SQLException {
		int realValue = mission(event, data_id);
		switch(condition) {
		case CONDITION_MORE:
			return realValue >= number;
		case CONDITION_LESS:
			return realValue <= number;
		default:
			throw new IllegalArgumentException("캠페인 미션중 condition 변수가 예외적인 값이 나옴 : " + condition);
		}
	}
	
	public int mission(int event, int data_id) throws SQLException {
		int realValue;
		switch(event) {
		case MISSION_RESOURCE:
			realValue = getResourceNumber(data_id);
			break;
		case MISSION_BUILDING:
			realValue = getBuildingNumber(data_id);
			break;
		case MISSION_WORLDMAP:
			realValue = getHistoryNumber(EVENT_LOCALTOWORLD);
			break;
		case MISSION_WAR:
			realValue = getHistoryNumber(EVENT_WAR);
			break;
		default:
			throw new IllegalArgumentException("캠페인의 적절하지 못한 이벤트타입");
		}
		return realValue;
	}
	
	public int getResourceNumber(int resource_id) throws SQLException {
		String resourceSQL = "select number from Resource where resource_id = ?";
		ResultSet resourceSet = ddb.query(resourceSQL, resource_id);
		resourceSet.next();
		return resourceSet.getInt("number");
	}
	
	public int getBuildingNumber(int building_kind_id) throws SQLException {
		String buildingSQL = "select count(*) from Building where building_kind_id = ?";
		ResultSet buildingSet = ddb.query(buildingSQL, building_kind_id);
		buildingSet.next();
		return buildingSet.getInt("count(*)");
	}
	
	public int getHistoryNumber(int event_type) throws SQLException {
		String historySQL = "select count(*) from History where event_type = ?";
		ResultSet historySet = ddb.query(historySQL, event_type);
		historySet.next();
		return historySet.getInt("count(*)");
	}
	
	public boolean onEvent(int event_type, int arg) throws SQLException {
		String eventSQL = "insert into(event_type, arg) values(?, ?)";
		return ddb.execute(eventSQL, event_type, arg);
	}
	
	public boolean onTestBuild(int building_kind_id, int map_id) throws SQLException {
		String buildSQL = "insert into(building_kind_id, map_id) values(?, ?)";
		return ddb.execute(buildSQL, building_kind_id, map_id);
	}
	
	public MessageData getCampaignLabel(DB sdb) throws SQLException {
		String userSQL = "select campaign_id, mission_id from Campaign";
		ResultSet userSet = ddb.query(userSQL);
		userSet.next();
		
		int campaign_id = userSet.getInt("campaign_id");
		int mission_id = userSet.getInt("mission_id");
		
		String staticSQL = "select name, description"
				+ " from CampaignMission"
				+ " where campaign_id = ?"
				+ " and mission_id = ?";
		ResultSet staticSet = sdb.query(staticSQL, campaign_id, mission_id);
		boolean value = staticSet.next();
		
		if(value == false) return null;
		String description = staticSet.getString("description");
		String name = "Quest : " + staticSet.getString("name");
		
		return new MessageData(name, 0, description, -1);
	}
}
