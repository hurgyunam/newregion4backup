package main.java.com.overtheinfinite.newregion.tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileTool {
	public List<String> read(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		List<String> list = new LinkedList<String>();
		while(br.ready()) {
			list.add(br.readLine());
		}
		br.close();
		return list;
	}
}
