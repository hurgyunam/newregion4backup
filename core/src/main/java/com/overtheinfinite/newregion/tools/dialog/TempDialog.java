package main.java.com.overtheinfinite.newregion.tools.dialog;

import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

//Skin, title, ok, cancel, dialoglistener
//dialoglistener의 한계 = 비동기, 내가 데이터를 가지고 있어야해
//하지만 상위 객체를 가지고 좌지우지 하는건 좋지 않아

public class TempDialog extends Dialog {
	//내가 데이터를 가지고 get/set으로 처리하는게 나을 텐데
	//데이터로 뭐가 들어갈지 너무 광범위함
	//그리고 여러 경우에 처리가 가능하도록 onTouch(MotionEvent event) 같은 식의 처리가
	//진행되길 원함
	private DialogListener listener;
	private int id;
	
	public TempDialog(DialogParameter param, Skin skin) {
		super(param.getTitle(), skin);
		
		button(param.getOKStr(), true);
		String cancel = param.getCancelStr();
		if(cancel != null) {
			button(param.getCancelStr(), false);
		}
		
		this.listener = param.getListener();
		this.id = param.getId();
	}
	
	public void result(Object input) {
		Boolean isOK = (Boolean) input;
		if(this.listener != null) {
			this.listener.onTouchButton(isOK, this.id);
		}		
	}
}
