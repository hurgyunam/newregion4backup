package main.java.com.overtheinfinite.newregion.tools.db;

import java.io.IOException;
import java.sql.SQLException;

public class DBM {
	private DB sdb, ddb;
	private final String SDB_NAME = "sdb.db", DDB_NAME = "ddb.db", CDB_NAME = "cdb.db";
	private final String SDB_SQL = "sqls.txt", DDB_SQL = "dsqls.txt", NORMAL_SQL = "normal_sql.txt", CAMPAIGN_SQL = "campaign_sql.txt"; 
	private static DBM instance;
	public void init(String ddbname, String dsqlfile) throws SQLException, IOException {
		sdb = new DB();
		ddb = new DB();
		
		if(sdb.init(SDB_NAME) == false) {
			sdb.readSQL(SDB_SQL);
		}
		if(ddb.init(ddbname) == false) {
			ddb.readSQL(DDB_SQL);
			ddb.readSQL(dsqlfile);
		}
	}
	
	public DB getStaticDB() {
		return sdb;
	}
	public DB getDynamicDB() {
		return ddb;
	}
	
	public void normalMode() {
		try {
			init(DDB_NAME, NORMAL_SQL);
		} catch (SQLException e) {
			throw new IllegalStateException("노말모드 실행 중 에러", e);
		} catch (IOException e) {
			throw new IllegalStateException("노말모드 실행 중 에러", e);
		}
	}
	
	public void campaignMode() {
		try {
			init(CDB_NAME, CAMPAIGN_SQL);
		} catch (SQLException e) {
			throw new IllegalStateException("캠페인모드 실행 중 에러", e);
		} catch (IOException e) {
			throw new IllegalStateException("캠페인모드 실행 중 에러", e);
		}
	}
	
	public static DBM getInstance() {
		if(instance == null) {
			instance = new DBM();
		}
		return instance;
	}
	
}
