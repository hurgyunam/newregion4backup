package main.java.com.overtheinfinite.newregion.tools.db;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

public class DB extends SQLiteManager {
	private static final int START_INDEX = 1;
	public int queryInteger(String sql, Object...args) throws SQLException {
		ResultSet set = this.query(sql, args);
		set.next();
		return set.getInt(START_INDEX);
	}

	public String queryString(String sql, Object...args) throws SQLException {
		ResultSet set = this.query(sql, args);
		set.next();
		return set.getString(START_INDEX);
	}
}
