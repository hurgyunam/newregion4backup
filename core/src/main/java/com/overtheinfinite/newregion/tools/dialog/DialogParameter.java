package main.java.com.overtheinfinite.newregion.tools.dialog;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class DialogParameter {
	private String title, ok, cancel;
	private DialogListener listener;
	private int id;
	public DialogParameter(String title, String ok, String cancel, DialogListener listener, int id) {
		super();
		this.title = title;
		this.ok = ok;
		this.cancel = cancel;
		this.listener = listener;
		this.id = id;
	}

	public DialogParameter(String title, String ok, String cancel, DialogListener listener) {
		this(title, ok, cancel, listener, 0);
	}
	
	public DialogParameter(String title, String ok) {
		this(title, ok, null, null, 0);
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getOKStr() {
		return ok;
	}
	
	public String getCancelStr() {
		return cancel;
	}
	
	public DialogListener getListener() {
		return listener;
	}
	
	public int getId() {
		return id;
	}
}
