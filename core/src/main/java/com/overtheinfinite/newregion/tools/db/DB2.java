package main.java.com.overtheinfinite.newregion.tools.db;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import main.java.com.overtheinfinite.newregion.tools.Logger;

@Deprecated
public class DB2 extends SQLiteManager {
	public static String DB_DYNAMIC = "ddb.db", DB_STATIC = "sdb.db";
	private static final int STARTING_INDEX = 1;
	private static Hashtable<String, DB2> managers = new Hashtable<String, DB2>();
	public static DB2 getInstance(String name) throws SQLException {
		if(managers.get(name) == null) {
			DB2 manager = new DB2();
			boolean isOld = manager.init(name);
			if(!isOld && name.equals(DB_DYNAMIC)) {
				try {
					manager.readSQL("dsqls.txt");
				} catch (IOException e) {
					throw new IllegalStateException("새로운 동적디비에 SQL문을 도입하는 과정에서 오류가 발생", e);
				}
			}
			managers.put(name, manager);
		}
		return managers.get(name);
	}
	public static void init(String ddbname, int type) throws SQLException, IOException {
		DB2 sdb = getInstance(DB2.DB_STATIC);
		DB2 ddb = getInstance(ddbname);
		switch(type) {
		case 1:
			Logger.getInstance().add("DB.init", "load sdb data");
			sdb.readSQL("sqls.txt");
		case 2:
			ddb.readSQL("dsqls.txt");
			break;			
		}
	}
	
	public static void init(int type) throws SQLException, IOException {
		init("ddb.db", type);
	}
	
	public static boolean delete(int type) throws SQLException, IOException {
		boolean isSuccess = true;
		switch(type) {
		case 1:
			SQLiteManager sdb = getInstance(DB2.DB_STATIC);
			isSuccess = isSuccess && sdb.delete();
		case 2:
			SQLiteManager ddb = getInstance(DB2.DB_DYNAMIC);
			isSuccess = isSuccess && ddb.delete();
			break;
		}
		return isSuccess;
	}
	
	public static String query(String name, String sql, Object...args) throws SQLException {
		DB2 db = getInstance(name);
		ResultSet set = db.query(sql, args);
		set.next();
		return set.getString(0);
	}
	
	public static int queryInteger(String name, String sql, Object...args) throws SQLException {
		DB2 db = getInstance(name);
		ResultSet set = db.query(sql, args);
		set.next();
		return set.getInt(STARTING_INDEX);
	}
}
