package main.java.com.overtheinfinite.newregion.tools.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import main.java.com.overtheinfinite.newregion.tools.Logger;

public class SQLiteManager {
	private Connection conn;
	private String dbFilename;
	private boolean isOpened = false;
	/**
	 * @param db
	 * @return is this db made first
	 * @throws SQLException
	 */
	public boolean init(String db) throws SQLException {
		this.dbFilename = db;
		boolean exists = exists();
		this.conn = DriverManager.getConnection("jdbc:sqlite:db\\" + dbFilename);
		isOpened = true;
		return exists;
	}
	public boolean delete() {
		try {
			this.conn.close();
			Files.delete(Paths.get("db\\" + dbFilename));
			Logger.getInstance().add("db.delete", dbFilename + " deletion ");
			init(dbFilename);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean exists() {
		File f = new File("db\\" + dbFilename);
		return f.exists();
	}
	
	public void close() throws SQLException {
		this.conn.close();
	}
	
	public boolean execute(String sql, Object...args) throws SQLException {
		PreparedStatement ps = makeStatement(sql, args);
		if(ps == null) return false;
		return ps.execute();
	}

	public ResultSet query(String sql, Object...args) throws SQLException {
		PreparedStatement ps = makeStatement(sql, args);
		if(ps == null) return null ;
		return ps.executeQuery();
	}
	
	public void readSQL(String filename) throws IOException, SQLException {
		Logger.getInstance().add("db.readSQL", filename + "->" + dbFilename);
		BufferedReader br = new BufferedReader(new FileReader("db\\" + filename));
		while(br.ready()) {
			String line = null;
			try {
				line = br.readLine();
				execute(line);
			}catch(IOException e) {
				throw new IOException("메시지를 읽던 중 에러 " + line, e);
			}
		}
		br.close();
	}
	
	public PreparedStatement makeStatement(String sql, Object...args) throws SQLException {
		if(isOpened == false) return null;
		
		PreparedStatement prep = this.conn.prepareStatement(sql);
		Logger.getInstance().add("sql", sql);
		Logger.getInstance().add("sql.args", "args size : " + args.length);
		for(int i = 0; i < args.length; i++) {
			Object arg = args[i];
			if(arg instanceof Integer) {
				Logger.getInstance().add("sql.args.loop", i+1 + " -> " + arg);
				prep.setInt(i+1, (Integer)arg);
			}
			else if(arg instanceof String) {
				prep.setString(i, (String)arg);
			}
			else if(arg instanceof List) {
				List list = (List) arg;
				StringBuffer sb = new StringBuffer("(");
				for(int k = 0; k < list.size(); k++) {
					sb.append(list.get(k));
					if(k != list.size() - 1)
						sb.append(",");
				}
				sb.append(")");
				prep.setString(i, sb.toString());
			}
		}

		return prep;
	}
	
	static {
		try {
			Class.forName("org.sqlite.JDBC");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
