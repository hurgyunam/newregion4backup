package main.java.com.overtheinfinite.newregion.tools.dialog;

import java.sql.SQLException;

public interface DialogListener {
	public void onTouchButton(boolean isOK, int id);
}
