package main.java.com.overtheinfinite.newregion.ui;

import java.sql.SQLException;
import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import main.java.com.overtheinfinite.newregion.controller.element.OptionButton;
import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.game.Builder;
import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.game.element.BuildButtonData;
import main.java.com.overtheinfinite.newregion.game.element.FunctionButtonData;
import main.java.com.overtheinfinite.newregion.game.element.ViewData;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.ui.element.BuildButton;
import main.java.com.overtheinfinite.newregion.ui.element.FunctionButton;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;
import main.java.com.overtheinfinite.newregion.ui.element.ViewLabel;

public class UIDrawer extends Window {
	private LinkedList<BuildButton> buildButtons = new LinkedList<BuildButton>();
	private ViewLabel[] labels = new ViewLabel[4];
	private OptionButton optionbtn;
	
	public UIDrawer(Skin skin, Builder builder) throws SQLException {
		super("", skin);
		setBounds(MapImage.XSIZE * 5, 0, Gdx.graphics.getWidth() - MapImage.XSIZE * 5, MapImage.YSIZE * 5);
		BuildButtonData[] btns = builder.getAllBuildingData();
		for(int i = 0; i < btns.length; i++) {
			buildButtons.add(new BuildButton(btns[i]));
		}
	}

	public void updateBuildButtons(GameStage stage, boolean isBuildCastle)  {
		clear();
		add(new ViewData(null, "자금", Resource.MONEY).toString()).colspan(3);
		row();
		
		for(int i = 0; i < buildButtons.size(); i++) {
			add(buildButtons.get(i));
			if(!isBuildCastle) {
				if(i != 1 && i != 3) {
					buildButtons.get(i).setTouchable(Touchable.disabled);
					buildButtons.get(i).setColor(0.2f, 0.2f, 0.2f, 1f);
				}
			}
			else {
				buildButtons.get(i).setTouchable(Touchable.enabled);
				buildButtons.get(i).setColor(1f, 1f, 1f, 1f);
			}
			if(i % 3 == 2)
				row();
		}
		
		row();
		if(this.optionbtn != null)
			add(this.optionbtn);
	}
	
	public void updateView() {
		for(int i = 0; i < labels.length; i++) {
			if(labels[i] != null)
				labels[i].update();
		}
	}
	
	public void clear() {
		super.clear();
	}
	
	//이건 소수 데이터 처리시 사용
	public void updateButton(Skin skin, ViewData[] views, FunctionButton[] btn) {
		clear();
		for(int i = 0; i < views.length; i++) {
			if(views[i] != null) {
				labels[i] = new ViewLabel(skin, views[i]);
				add(labels[i]);
				row();
			}
		}
		add("   ");
		row();
		add("   ");
		row();
		for(int i = 0; i < btn.length; i++) {
			add(btn[i]);
			row();
		}
		
		if(optionbtn != null)
			add(optionbtn);
	}
}
