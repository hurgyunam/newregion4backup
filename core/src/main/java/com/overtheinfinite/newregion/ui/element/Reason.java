package main.java.com.overtheinfinite.newregion.ui.element;

public class Reason {
	private String reason;
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getReason() {
		return reason;
	}
	
	public String toString() {
		return reason;
	}
}
