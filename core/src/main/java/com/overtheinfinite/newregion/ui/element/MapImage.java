package main.java.com.overtheinfinite.newregion.ui.element;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.game.element.TileData;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;

public class MapImage extends Image {
	private TileData data;
	public static int XSIZE, YSIZE;
	//이곳에 사실상 TileData를 받는게 좋음
	public MapImage(Texture tex, TileData data) {
		super(tex);
		this.data = data;
		this.setBounds(data.getX() * XSIZE, 
				data.getY() * YSIZE, 
				XSIZE, YSIZE);
	}
	
	public int getTileX() {
		return data.getX();
	}
	
	public int getTileY() {
		return data.getY();
	}
	
	public TileData getData() {
		return data;
	}
}
