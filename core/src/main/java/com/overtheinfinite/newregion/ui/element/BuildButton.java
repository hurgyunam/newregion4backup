package main.java.com.overtheinfinite.newregion.ui.element;

import java.sql.SQLException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.controller.stage.GameStage;
import main.java.com.overtheinfinite.newregion.game.Builder;
import main.java.com.overtheinfinite.newregion.game.Resource;
import main.java.com.overtheinfinite.newregion.game.element.BuildButtonData;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.tools.dialog.DialogListener;
import main.java.com.overtheinfinite.newregion.ui.MapDrawer;

public class BuildButton extends Image {
	private BuildButtonData data;
	public BuildButton(BuildButtonData btn) {
		super(new Texture(Gdx.files.internal(btn.getFilename())));
		this.data = btn;
		this.setTouchable(btn.isEnable()? Touchable.enabled : Touchable.disabled);
		setSize(300, 300);
	}
	
	public BuildButtonData getData() {
		return data;
	}
	
	public String toString() {
		try {
			String msg = data.getLabel() + "을 설치하시겠습니까? ("
					+ data.getPrice() + "/" + data.getMyPrice()
					+ "G)";
			return msg;
		} catch (SQLException e) {
			throw new IllegalArgumentException("건설 버튼 다이얼로그 확인 창을 위해 데이터를 불러오던 중 에러 발생", e);
		}
	}
}
