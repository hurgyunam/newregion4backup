package main.java.com.overtheinfinite.newregion.ui;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.element.BuildingData;
import main.java.com.overtheinfinite.newregion.game.element.TileData;
import main.java.com.overtheinfinite.newregion.tools.FileTool;
import main.java.com.overtheinfinite.newregion.tools.db.DB;
import main.java.com.overtheinfinite.newregion.ui.element.MapImage;

public class MapDrawer extends Group {
	public static final int GRASS = 1, DIRT = 2, STONE = 3, SAND = 4;
	public static final int TILE_GRASS = 15;
	public static final int TILE_X_NUM = 7, TILE_Y_NUM = 5;
	private int currentMap = 1;
	private List<Image> images;
	private Texture[] tex;
	private MapParser parser;
	
	//connected to uidrawers
	public MapDrawer() {
		tex = new Texture[4];
		tex[0] = new Texture("tile/grass.png");
		tex[1] = new Texture("tile/dirt.png");
		tex[2] = new Texture("tile/stone.png");
		tex[3] = new Texture("tile/sand.png");
		
		images = new LinkedList<Image>();
		
		MapImage.XSIZE = Gdx.graphics.getWidth() / TILE_X_NUM;
		MapImage.YSIZE = Gdx.graphics.getHeight() / TILE_Y_NUM;
		
		this.parser = new MapParser();
		try {
			this.parser.read("map/region.txt");
			this.parser.readWorld("map/world.txt");
		} catch (IOException e) {
			throw new IllegalStateException("지역 텍스트 데이터를 읽어오는데 에러가 발생", e);
		}
	}
	
	public void update(int map_id) {
		clear();
		currentMap = map_id;
		updateImage(map_id);
	}
	
	public void update() {
		update(currentMap);
	}
	
	private void updateImage(int map_id) {
		String[] map = parser.getMap(map_id);
		for(int i = 0; i < map.length; i++) {
			for(int j = 0; j < map[i].length(); j++) {
				int data = Integer.parseInt(map[i].charAt(j) + "");
				int tile_map_id = (map_id == 0)? 5 * i + j + 1 : map_id;
				
				TileData tile = new TileData(j, i, data, tile_map_id);
				Image image = new MapImage(tex[data-1], tile);
				images.add(image);
				addActor(image);
			}
		}
	}
}
