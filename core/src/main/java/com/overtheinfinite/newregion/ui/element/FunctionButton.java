package main.java.com.overtheinfinite.newregion.ui.element;

import java.sql.SQLException;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import main.java.com.overtheinfinite.newregion.game.Building;
import main.java.com.overtheinfinite.newregion.game.element.FunctionButtonData;
import main.java.com.overtheinfinite.newregion.tools.Logger;
import main.java.com.overtheinfinite.newregion.ui.UIDrawer;

public class FunctionButton extends TextButton {
	private FunctionButtonData btn;
	
	public FunctionButton(FunctionButtonData btn, Skin skin) {
		super(btn.getLabel(), skin);
		this.btn = btn;
		this.setSize(200, 50);
	}
	
	public FunctionButtonData getButtonData() {
		return btn;
	}
}
