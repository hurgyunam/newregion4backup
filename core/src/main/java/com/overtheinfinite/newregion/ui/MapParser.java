package main.java.com.overtheinfinite.newregion.ui;

import java.io.IOException;
import java.util.List;

import main.java.com.overtheinfinite.newregion.tools.FileTool;

public class MapParser {
	private FileTool tool = new FileTool();
	private String[][][] mapData = new String[5][5][5];
	private String[] worldData = new String[5];
	public void read(String filename) throws IOException {
		List<String> list = tool.read(filename);
		for(int i = 0; i < list.size(); i++) {
			if(i % 6 == 5) i++;
			String line = list.get(i);
			String[] datas = line.split(" ");
			for(int j = 0; j < datas.length; j++) {
				mapData[i / 6][j][i % 6] = datas[j];
			}
		}
	}
	
	public void readWorld(String filename) throws IOException {
		List<String> list = tool.read(filename);
		list.toArray(worldData);
	}
	
	public String[] getMap(int x, int y) {
		return mapData[y][x];
	}
	
	public String[] getMap(int map_id) {
		//0은 월드맵. 나머지의 경우 처리를 해줘야함
		if(map_id == 0) {
			return worldData;
		}
		else {
			int temp = map_id - 1;
			int x = temp % 5;
			int y = temp / 5;
			return getMap(x,y);
		}
	}
	
	public static void main(String[] args) throws IOException {
		MapParser parser = new MapParser();
		parser.read("map/region.txt");
		
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				String[] map = parser.getMap(i, j);
				System.out.println(i + "/" + j);
				for(int k = 0; k < 5; k++) {
					System.out.println(map[k]);
				}
			}
		}
	}
}
