package main.java.com.overtheinfinite.newregion.ui.element;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import main.java.com.overtheinfinite.newregion.game.element.ViewData;

public class ViewLabel extends Label {
	private ViewData view;
	public ViewLabel(Skin skin, ViewData view) {
		super(view.toString(), skin);
		this.view = view;
	}
	
	public void update() {
		this.setText(view.toString());
	}
}